package com.example.asus.myweather.Model;

/**
 * Created by ASUS on 10/14/2017.
 */

public class Clouds {
    private int all;

    public Clouds(int all)  {
        this.all = all;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
