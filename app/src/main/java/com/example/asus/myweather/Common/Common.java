package com.example.asus.myweather.Common;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ASUS on 10/13/2017.
 */

public class Common {
    public static String API_KEY ="02da9b0eb9cdebc6734ba7c2be3339aa";
    public static String API_LINK ="http://api.openweathermap.org/data/2.5/weather/";

    @NonNull
    public static String apiRequest(String lat, String lng) {
        StringBuilder sb = new StringBuilder(API_LINK);
        sb.append(String.format("?lat=%s&lon=%s&APPID=%s&units-metric",lat,lng,API_KEY));
        return sb.toString();
    }

    public static String unixTimesStampToDate(double unixTimeStamp) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        date.setTime((long)unixTimeStamp*1000);
        return dateFormat.format(date);
    }

    public static String getImage(String icon){
        return String.format("https://openweathermap.org/img/w/04d.png",icon);
    }

    public static String getDateNow(){
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
